package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping (value="/admin")
public class AdminController {
	
	@GetMapping(value="")
	public ModelAndView homePage() {
		ModelAndView mav  = new ModelAndView("admin/adminPage");
		return mav;
	}

	@RequestMapping(value="/product")
	public ModelAndView productList() {
		ModelAndView mav  = new ModelAndView("admin/productList");
		return mav;
	}
	
	@RequestMapping(value="/productAdd")
	public ModelAndView productAdd() {
		ModelAndView mav  = new ModelAndView("admin/productAdd");
		return mav;
	}

}
