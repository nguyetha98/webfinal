package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class AccountController {
	
	@GetMapping(value = "/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("login/login");
		return mav;
	}

	@RequestMapping(value = "/register")
	public ModelAndView register() {
		ModelAndView mav = new ModelAndView("login/register");
		return mav;
	}
	@GetMapping(value = "/forgotPass")
	public ModelAndView forgotPass() {
		ModelAndView mav = new ModelAndView("login/forgotPass");
		return mav;
	}
	@GetMapping(value = "/confirm_register")
	public ModelAndView confirm_register() {
		ModelAndView mav = new ModelAndView("login/confirm_register");
		return mav;
	}
	@GetMapping(value = "/confirm")
	public ModelAndView confirm() {
		ModelAndView mav = new ModelAndView("login/confirm");
		return mav;
	}
	@GetMapping(value = "/changePassword")
	public ModelAndView changePassword() {
		ModelAndView mav = new ModelAndView("login/changePassword");
		return mav;
	}

}
