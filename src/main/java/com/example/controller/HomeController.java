package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {
	
	@GetMapping(value="/")
	public ModelAndView homePage() {
		ModelAndView mav  = new ModelAndView("user/index");
		return mav;
	}
	
	@RequestMapping(value="/gallery")
	public ModelAndView aboutPage() {
		ModelAndView mav  = new ModelAndView("user/gallery");
		return mav;
	}
	
	@RequestMapping(value="/categories")
	public ModelAndView categoriesPage() {
		ModelAndView mav  = new ModelAndView("user/categories");
		return mav;
	}
	
	@RequestMapping(value="/contact")
	public ModelAndView contactPage() {
		ModelAndView mav  = new ModelAndView("user/contact");
		return mav;
	}
	
	//test view
	@RequestMapping(value="/test")
	public ModelAndView testPage() {
		ModelAndView mav  = new ModelAndView("user/galleryDetail");
		return mav;
	}
}
