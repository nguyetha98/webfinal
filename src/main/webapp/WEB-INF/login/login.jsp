<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags-->
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Colorlib Templates">
<meta name="author" content="Colorlib">
<meta name="keywords" content="Colorlib Templates">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Title Page-->
<title>Au Register Forms by Colorlib</title>

<!-- Icons font CSS-->
<link href="/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link href="/vendor/font-awesome-4.7/css/font-awesome.min.css
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="/vendor/datepicker/daterangepicker.css" rel="stylesheet"
	media="all">
<!-- Main CSS-->
<link href="/css/main.css" rel="stylesheet" media="all">

<script type="application/x-javascript">
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 


</script>

<style type="text/css">
.linkform {
	text-decoration: none;
	padding-left: 253px;
	color: #84b4cb;
}

.linkform:hover {
	color: #a02c2d;
}

.forgotpass, .changepass {
	text-decoration: none;
	color: #a02c2d;
}

.forgotpass:hover, .changepass:hover {
	color: #84b4cb;
}

.changepass {
	padding-left: 220px;
}
</style>

</head>
<script>
	$(document).ready(function() {

		$('#myForm').bind({
			'submit' : function() {

				if ($('#email').val() == '') {
					$("#email").css("border", "1px solid red");
					$('#error_email').html('Bạn chưa nhập email');
					return false;
				}

				if ($('#password').val() == '') {
					$("#password").css("border", "1px solid red");
					$('#error_password').html('Bạn chưa nhập mật khẩu');
					return false;
				}

				return true;
			},
			'keydown' : function() {
				if ($('#email').val().length > 0) {
					$("#email").css("border", "1px solid green");
					$('#error_email').html('');
				}

				if ($('#password').val().length > 0) {
					$("#password").css("border", "1px solid green");
					$('#error_password').html('');
				}

			}
		});
	});
</script>
<body>
	<div class="page-wrapper bg-red p-t-180 p-b-100 font-robo">
		<div class="wrapper wrapper--w960">
			<div class="card card-2">
				<div class="card-heading"></div>
				<div class="card-body">
					<h2 class="title">Login</h2>
					<form action="/Xulydangnhap"
						id="myForm" method="POST">
						<div class="input-group">
							<input class="input--style-2" type="text" placeholder="Email"
								name="email" id="email">
							<p id="error_email" style="color: red;"></p>
						</div>
						<div class="input-group">
							<input class="input--style-2" type="password"
								placeholder="Password" name="password" id="password">
							<p id="error_password" style="color: red;"></p>
						</div>
						<p style="color: red;">
							
						</p>
						<div class="p-t-30">
							<button class="btn btn--radius btn--green" type="submit">Login</button>
							<a class="linkform" href="${pageContext.request.contextPath}/register">Create
								an account</a>
						</div>
						<div class="p-t-30"
							style="border-top: 1px solid #e5e5e5; margin-top: 20px;">
							<a class="forgotpass"
								href="${pageContext.request.contextPath}/forgotPass"
                            "><i
								class="fa fa-arrow-left" aria-hidden="true"></i> Forgot password</a>
							<!-- <a class="changepass" href="login_register/changePassword.jsp">Change password -->
							<i class="fa fa-arrow-right" aria-hidden="true"></i></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Jquery JS-->
	<script src="/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="/vendor/select2/select2.min.js"></script>
	<script src="/vendor/datepicker/moment.min.js"></script>
	<script src="/vendor/datepicker/daterangepicker.js"></script>

	<!-- Main JS-->
	<script src="/js/global.js"></script>

</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->