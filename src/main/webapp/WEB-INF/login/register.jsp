<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags-->
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Colorlib Templates">
<meta name="author" content="Colorlib">
<meta name="keywords" content="Colorlib Templates">

<!-- Title Page-->
<title>Register</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
<script src="templates/login/js/jquery.min.js"></script>
<script type="application/x-javascript">

	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!-- Icons font CSS-->
<link href="/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link href="/vendor/font-awesome-4.7/css/font-awesome.min.css
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="/vendor/datepicker/daterangepicker.css" rel="stylesheet"
	media="all">
<!-- Main CSS-->
<link href="/css/main.css" rel="stylesheet" media="all">

<style type="text/css">
.linkform {
	text-decoration: none;
	padding-left: 253px;
	color: #84b4cb;
}

.linkform:hover {
	color: #a02c2d;
}
</style>

</head>
<%-- <%
	ArrayList<KhachHang> list = KhachHangDao.getList();
%> --%>
<script>
        $(document).ready(function() {
          
            $('#myForm').bind({
                'submit': function() {
                 
                  
                    if ($('#email').val()=='') {
                    	$("#email").css("border", "1px solid red");
                        $('#error_email').html('Bạn chưa nhập email');
                        return false;
                    }

                    var email = $('#email').val();
                    var listEmail = [];
<%--                     <%for (KhachHang val : list) {%>
 --%>   <%--                  listEmail.push('<%=val.getEmail()%>');
<%}%> --%>
			if (listEmail.indexOf(email) > -1) {
									$("#email").css("border", "1px solid red");
									$('#error_email').html(
											'Email này đã được đăng ký sử dụng');
									return false;
								}
								if ($('#tenkhachhang').val() == '') {
									$("#tenkhachhang").css("border", "1px solid red");
									$('#error_tenkhachhang').html(
											'Bạn chưa nhập tên khách hàng');
									return false;
								}

								if ($('#password').val() == '') {
									$("#password").css("border", "1px solid red");
									$('#error_password').html(
											'Bạn chưa nhập mật khẩu');
									return false;
								}
								if ($('#confirm_password').val() != $(
										'#password').val()) {
									$("#confirm_password").css("border", "1px solid red");
									$('#error_confirm_password').html(
											'Mật khẩu không trùng');
									return false;
								}

								return true;
							},
							'keydown' : function() {
								if ($('#email').val().length > 0) {
									$("#email").css("border", "1px solid green");
									$('#error_email').html('');
								}
								if ($('#tenkhachhang').val().length > 0) {
									$("#tenkhachhang").css("border", "1px solid green");
									$('#error_tenkhachhang').html('');
								}
								if ($('#password').val().length > 0) {
									$("#password").css("border", "1px solid green");
									$('#error_password').html('');
								}
								if ($('#confirm_password').val() == $(
										'#password').val()) {
									$("#confirm_password").css("border", "1px solid green");
									$('#error_confirm_password').html('');

								}

							}
						});
			});
</script>
<body>

	<div class="page-wrapper bg-red p-t-180 p-b-100 font-robo">
		<div class="wrapper wrapper--w960">
			<div class="card card-2">
				<div class="card-heading"></div>
				<div class="card-body">
					<h2 class="title">Register</h2>
					<form action="<%=request.getContextPath()%>/XuLyDangKy" id="myForm"
						method="POST">
						<div class="input-group">
							<input class="input--style-2" type="text" placeholder="Email"
								name="email" id="email">
								<p id="error_email" style="color: red;"></p>
						</div>
						<div class="input-group">
							<input class="input--style-2" type="text" placeholder="Name"
								name="tenkhachhang" id ="tenkhachhang" >
								<p id="error_tenkhachhang" style="color: red;"></p>
						</div>
						<div class="row row-space">
							
							<div class="col-2">
								<div class="input-group">
									<div class="rs-select2 js-select-simple select--no-search">
										<select name="gioitinh">
											<option disabled="disabled" selected="selected">Gender</option>
											<option>Male</option>
											<option>Female</option>
											<!-- <option>Other</option> -->
										</select>
										<div class="select-dropdown"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="input-group">
							<input class="input--style-2" type="password"
								placeholder="Password" name="password" id="password">
								<p id="error_password" style="color: red;"></p>
						</div>
						<div class="input-group">
							<input class="input--style-2" type="password"
								placeholder="Comfirm Password" name="confirm_password" id="confirm_password"
						 name="confirm_password">
						 <p id="error_confirm_password" style="color: red;"></p>
						</div>
						<div class="p-t-30">
							<button class="btn btn--radius btn--green" type="submit">Register</button>
							<a class="linkform" href="${pageContext.request.contextPath}/login">I am
								already member</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Jquery JS-->
	<script src="/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="/vendor/select2/select2.min.js"></script>
	<script src="/vendor/datepicker/moment.min.js"></script>
	<script src="/vendor/datepicker/daterangepicker.js"></script>

	<!-- Main JS-->
	<script src="/js/global.js"></script>

</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->