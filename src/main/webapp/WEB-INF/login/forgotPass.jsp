
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags-->
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Colorlib Templates">
<meta name="author" content="Colorlib">
<meta name="keywords" content="Colorlib Templates">

<!-- Title Page-->
<title></title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<!-- Icons font CSS-->
<link href="/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link href="/vendor/font-awesome-4.7/css/font-awesome.min.css
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="/vendor/datepicker/daterangepicker.css" rel="stylesheet"
	media="all">
<!-- Main CSS-->
<link href="/css/main.css" rel="stylesheet" media="all">

<style type="text/css">
.linkform {
	text-decoration: none;
	padding-left: 253px;
	color: #84b4cb;
}

.linkform:hover {
	color: #a02c2d;
}

.forgotpass, .changepass {
	text-decoration: none;
	color: #a02c2d;
}

.forgotpass:hover, .changepass:hover {
	color: #84b4cb;
}

.changepass {
	padding-left: 220px;
}
</style>

</head>

<script>
        $(document).ready(function() {
        	 function validateEmail(sEmail) {
                 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                 if (filter.test(sEmail)) {
                     return true;
                 } else {
                     return false;
                 }
             }
            $('#myForm').bind({
                'submit': function() {
                   /*  if (!validateEmail($('#tenkhachhang').val())) {
                        $('#error_tenkhachhang').html('Bạn chưa nhập tên khách hàng');
                        return false;
                    } */
                    if ($('#email').val()=='') {
                        $('#error_email').html('Bạn chưa nhập email');
                        return false;
                    }
                    if (!validateEmail($('#email')
							.val())) {
						$('#error_email')
								.html(
										'Email không đúng định dạng');
						return false;
					}
                   
                    var email = $('#email').val();
                    var listEmail = [];
                   
	if (listEmail.indexOf(email) == -1) { // kiểm tra maqg(mã quốc gia) mà người dùng nhập có nằm trong listMaQuocGia hay không. Nếu không có trả về -1
													$('#error_email')
															.html(
																	'Email không tồn tại trong hệ thống');
													return false;
												}
												return true;
											},
											'keydown' : function() {
												if ($('#email').val().length > 0) {
													$('#error_email').html('');
												}

											}
										});
					});
</script>
<body>
	<div class="page-wrapper bg-red p-t-180 p-b-100 font-robo">
		<div class="wrapper wrapper--w960">
			<div class="card card-2">
				<div class="card-heading"></div>
				<div class="card-body">
					<h2 class="title">Xác nhận Email</h2>
					<form action="<%=request.getContextPath()%>/DoiMatkhauController"
						id="myForm" method="POST">
						<div class="input-group">
							<input class="input--style-2" type="text" placeholder="Email"
								name="email" id="email">
								<p id="error_email" style="color: red;"></p>
						</div>
						<div style="display: none">
							<input type="text" class="form-control" id="action" name="action"
								value="nhapemail" onkeydown="">
						</div>
						<div class="p-t-30">
							<button class="btn btn--radius btn--green" type="submit">Xác
								nhận</button>

						</div>
						<div class="p-t-30"
							style="border-top: 1px solid #e5e5e5; margin-top: 20px;">
							<!--  <a class="forgotpass" href=""><i class="fa fa-arrow-left" aria-hidden="true"></i> Forgot password</a> -->

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Jquery JS-->
	<script src="/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="/vendor/select2/select2.min.js"></script>
	<script src="/vendor/datepicker/moment.min.js"></script>
	<script src="/vendor/datepicker/daterangepicker.js"></script>

	<!-- Main JS-->
	<script src="/js/global.js"></script>

</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->