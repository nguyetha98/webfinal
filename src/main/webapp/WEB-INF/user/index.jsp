<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>H3N2M Photos</title>

<!-- Favicon -->
<link rel="icon" href="img/core-img/favicon.ico">

<!-- Core Stylesheet -->
<link href="style.css" rel="stylesheet">


<!-- Responsive CSS -->
<link href="css/responsive/responsive.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>


<body>

	<%@ include file="/WEB-INF/fragment/header.jsp" %>

	</section>
	<!-- ****** Welcome Area End ****** -->


	<!-- ****** Categories Area End ****** -->

	<!-- ****** Blog Area Start ****** -->
	<div class="breadcumb-area"
		style="background-image: url(img/bg-img/breadcumb.jpg);">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12">
					<div class="bradcumb-title text-center">
						<h2>Home Page</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcumb-nav">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i
									class="fa fa-home" aria-hidden="true"></i> Home</a></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>


	<section class="blog_area section_padding_0_80">
		<div class="container" , style="margin-top: 75px">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<div class="row">

						<!-- Single Post -->
						<div class="col-12">
							<div class="single-post wow fadeInUp" data-wow-delay=".2s">

								<div class="post-thumb">
									
									<a href="${pageContext.request.contextPath}/test">
									<img src="img/blog-img/1.jpg" alt="girl"></a>
								</div>

								<div class="post-content">
									<div class="post-meta d-flex">
										<div class="post-author-date-area d-flex">

											<div class="post-author">
												<a href="#">Author</a>
											</div>

											<div class="post-date">
												<a href="#">10/07/2020</a>
											</div>
										</div>
										<div class="post-comment-share-area d-flex">

											<div class="post-favourite">
												<a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>
													</a>
											</div>

											<div class="post-comments">
												<a href="#"><i class="fa fa-comment-o"
													aria-hidden="true"></i></a>
											</div>
											<!-- Post Share -->

											<div class="post-share">
												<a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>


					</div>
				</div>
				<!-- ****** Blog Sidebar ****** -->
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">
					<div class="blog-sidebar mt-5 mt-lg-0">

						<div class="single-widget-area popular-post-widget"style="padding-left: 40px">
							<div class="widget-title text-center"style="width: 300px;">
								<h6>Popular Post</h6>
							</div>
							<!-- Single Post -->
							<div class="single-populer-post d-flex">
							<a href="">
								<img src="img/sidebar-img/1.jpg" alt="" style="width: 250px;"></a>
								<div class="post-content" style="padding-top: 80px;">
									<div class="post-favourite">
										<a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>
											</a>
									</div>
									<div class="post-share">
										<a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		</div>
	</section>
	<!-- ****** Blog Area End ****** -->

	<!-- ****** Instagram Area Start ****** -->
	<div
		class="instargram_area owl-carousel section_padding_100_0 clearfix"
		id="portfolio">

		<!-- Instagram Item -->
		<div class="instagram_gallery_item">
			<!-- Instagram Thumb -->
			<img src="" alt="">
			<!-- Hover -->
			<div class="hover_overlay">
				<div class="yummy-table">
					<div class="yummy-table-cell">
						<div class="follow-me text-center">
							<a
								href="">
								</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- ****** Our Creative Portfolio Area End ****** -->

	


	<!-- Jquery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap-4 js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins JS -->
	<script src="js/others/plugins.js"></script>
	<!-- Active JS -->
	<script src="js/active.js"></script>
	
	<div> <p></p></div>
	
	<%@ include file="/WEB-INF/fragment/footer.jsp" %>
</body>
