<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>H3N2M Photos</title>

<!-- Favicon -->
<link rel="icon" href="img/core-img/favicon.ico">

<!-- Core Stylesheet -->
<link href="style.css" rel="stylesheet">

<!-- Responsive CSS -->
<link href="css/responsive/responsive.css" rel="stylesheet">


<style type="text/css">
td {
	text-align: right;
	padding-left: 90px;
}

table {
	margin-left: 20px;
}

.download {
	border-top: 1px solid #ebebeb;
	padding-top: 0px;
}

.btndown {
	border: 1px solid #fc6c3f;
	border-radius: 20px;
	background: #fc6c3f;
	color: #fff;
	width: 800px;
	padding: 7px 50px 7px 50px;
	text-align: center;
}

.btndown:hover {
	border: 1px solid #fc6c3f;
	border-radius: 20px;
	background: #fff;
	color: #fc6c3f;
}
</style>
</head>

<body>
<%@ include file="/WEB-INF/fragment/header.jsp" %>

	<!-- ****** Breadcumb Area Start ****** -->
	<div class="breadcumb-area"
		style="background-image: url(img/bg-img/breadcumb.jpg);">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12">
					<div class="bradcumb-title text-center">
						<h2>Gallery Detail Page</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcumb-nav">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.jsp"><i
									class="fa fa-home" aria-hidden="true"></i> Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Gallery
								Detail Page</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<!-- ****** Breadcumb Area End ****** -->

	<!-- ****** Single Blog Area Start ****** -->
	<section class="single_blog_area section_padding_80">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<div class="row no-gutters">

						<!-- Single Post -->
						<div class="col-10 col-sm-11">
							<div class="single-post">
								<!-- Post Thumb -->

								<div class="post-thumb">
									<img src="img/blog-img/1.jpg" alt="">
								</div>
								<!-- Post Content -->
								<div class="post-content">
									<div class="post-meta d-flex">
										<div class="post-author-date-area d-flex">
											<!-- Post Author -->
											<a href="#">Author</a>
										</div>
										<div class="download">
											<a
												href=""
												class="btndown"> <i style="padding-right: 5px;"
												class="fa fa-download" aria-hidden="true"></i> Download
											</a>
										</div>
									</div>
									<!-- <a href="#">
										<h2 class="post-headline">Image name</h2>
									</a>
									<p>Image description</p> -->

								</div>
							</div>

							<!-- Related Post Area -->
							<div class="related-post-area section_padding_50"
								style="border-top: 1px solid #ebebeb;">
								<h4 class="mb-30">Related photo</h4>
								<div class="related-post-slider owl-carousel">
									<!-- Single Related Post-->
									<div class="single-post">
										<!-- Post Thumb -->
										<div class="post-thumb">
											<a
												href="">
												<img src="" alt="">
											</a>
										</div>
										<!-- Post Content -->
										<div class="post-content">
											<div class="post-meta d-flex">
												<div class="post-author-date-area d-flex">
													<!-- Post Author -->
													<div class="post-author">
														<a href="#"><i class="fa fa-heart-o"
															aria-hidden="true"></i>22</a>
													</div>
													<!-- Post Date -->
													<div class="post-date">
														<a href="#"><i class="fa fa-download"
															aria-hidden="true"></i>22</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- Comment Area Start -->
							<div class="comment_area section_padding_50 clearfix">

								<ol>
									<!-- Single Comment Area -->
									<li class="single_comment_area">

										<div class="comment-wrapper d-flex">
											<!-- Comment Meta -->
											<div class="fb-comments"
												data-href="http://localhost:8080/ThuongMai/galleryDetail.jsp"
												data-width="" data-numposts="5"></div>
										</div>
									</li>
								</ol>
							</div>

						</div>
					</div>
				</div>

				<!-- ****** Blog Sidebar ****** -->

				<div class="col-12 col-sm-8 col-md-6 col-lg-4">
					<div class="blog-sidebar mt-5 mt-lg-0">
						<!-- Single Widget Area -->

						<div class="single-widget-area about-me-widget text-center">
							<div class="widget-title">
								<h6>About Author</h6>
							</div>
							<div class="about-me-widget-thumb">
								<img src="" alt="">
							</div>
							<%-- <h4 class="font-shadow-into-light">
								<h6><%= kh1.getTenKhachHang() %></h6>
							</h4> --%>

						</div>
						<div class="single-widget-area subscribe_widget text-center">
							<div class="widget-title">
								<h6>Ngoc</h6>
							</div>
						</div>
						<!-- Single Widget Area -->
						<div class="single-widget-area subscribe_widget text-center">

							<div class="subscribe-link">
								<h3 class="widget_title">
									<i class='fa fa-camera-retro' style="padding-right: 15px;"></i>Nikon
									D5300
								</h3>
								<table style="padding-top: 10px; height: 10px;">
									<tr>
										<th>Type:</th>
										<td>jpg</td>
									</tr>
									<tr>
										<th>Resolution:</th>
										<td>aaaa</td>
									</tr>
									<tr>
										<th>Upload Date:</th>
										<td>22/12/2020</td>
									</tr>
									<tr>
										<th>Cost:</th>
										<td>22$</td>
									</tr>
									<tr>
										<th>Likes:</th>
										<td>22</td>
									</tr>
									<tr>
										<th>Downloads:</th>
										<td>22</td>
									</tr>
								</table>
							</div>
						</div>

						<!-- Single Widget Area -->
						<div class="single-widget-area popular-post-widget">
							<div class="widget-title text-center">
								<h6>More Images</h6>
							</div>
							<!-- Single Popular Post -->

							<!-- Single Post -->
							<div class="col-12">
								<div class="single-post wow fadeInUp" data-wow-delay=".2s">

									<div class="post-thumb">
										<a
											href="">
											<img src="img/blog-img/1.jpg" alt="girl">
										</a>
									</div>

									<div class="post-content">
										<div class="post-meta d-flex">
											<div class="post-author-date-area d-flex">

												<div class="post-author">
													<a href="#">author</a>
												</div>

												<div class="post-date">
													<a href="#">22/12/2020</a>
												</div>
											</div>
											<div class="post-comment-share-area d-flex">

												<div class="post-favourite">
													<a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>
														22</a>
												</div>

												<div class="post-comments">
													<a href="#"><i class="fa fa-comment-o"
														aria-hidden="true"></i>22</a>
												</div>
												<!-- Post Share -->

												<div class="post-share">
													<a href="#"><i class="fa fa-download"
														aria-hidden="true"></i>22</a>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ****** Single Blog Area End ****** -->

	<!-- ****** Instagram Area Start ****** -->
	
	<!-- ****** Our Creative Portfolio Area End ****** -->
	<div>
		<p></p>
	</div>
	
	<%@ include file="/WEB-INF/fragment/footer.jsp" %>

	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous"
		src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v5.0&appId=800840773719624&autoLogAppEvents=1"></script>

	<!-- Jquery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap-4 js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins JS -->
	<script src="js/others/plugins.js"></script>
	<!-- Active JS -->
	<script src="js/active.js"></script>
</body>